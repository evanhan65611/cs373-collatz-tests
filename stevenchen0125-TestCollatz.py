#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length, optimized_with_cache

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    def test_read_2(self):
        s = "12345 54321\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 12345)
        self.assertEqual(j, 54321)
    def test_read_3(self):
        s = "37833 359285\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 37833)
        self.assertEqual(j, 359285)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        v = collatz_eval(100000, 100000)
        self.assertEqual(v, 129)

    # --------------------
    # optimized_with_cache
    # --------------------

    def test_optimized(self):
        v = optimized_with_cache(280752,689833)
        self.assertEqual(v,509)
    def test_optimized_1(self):
        v = optimized_with_cache(90760,269530)
        self.assertEqual(v,443)
    def test_optimized_2(self):
        v = optimized_with_cache(593575,82223)
        self.assertEqual(v,470)
    def test_optimized_3(self):
        v = optimized_with_cache(305000, 307000)
        self.assertEqual(v,309)

    # ------------
    # cycle_length
    # ------------
    def test_cycle_length(self):
        v = cycle_length(1)
        self.assertEqual(v, 1)
    def test_cycle_length_1(self):
        v = cycle_length(9595)
        self.assertEqual(v, 74)
    def test_cycle_length_2(self):
        v = cycle_length(777777)
        self.assertEqual(v, 155)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 12345, 54321, 340)
        self.assertEqual(w.getvalue(), "12345 54321 340\n")
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 555555, 777777, 509)
        self.assertEqual(w.getvalue(), "555555 777777 509\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    def test_solve_2(self):
        r = StringIO("1234 4321\n6789 9876\n465 654\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1234 4321 238\n6789 9876 260\n465 654 145\n")
    def test_solve_3(self):
        r = StringIO("123789 321987\n3728 8457\n2934 5429\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "123789 321987 443\n3728 8457 262\n2934 5429 238\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
